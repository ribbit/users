import awsgi

from users.v1.v1_api import create

APP = create()


def lambda_handler(event, context):
    return awsgi.response(APP, event, context)  # pragma: no cover
