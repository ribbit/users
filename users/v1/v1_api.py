from datetime import datetime

import falcon
from falcon import Request, Response, API
from marshmallow import Schema, fields, ValidationError

from users.v1.user import User


class UserSchema(Schema):
    id = fields.String(attribute="uuid", required=True, dump_only=True)
    display_name = fields.String(required=True)
    created = fields.DateTime(required=True, dump_only=True, format="iso8601")
    last_login = fields.DateTime(required=True, dump_only=True, format="iso8601")


class MyAccountResource(object):

    schema = UserSchema()

    def on_post(self, req: Request, _resp: Response):
        try:
            display_name = self.schema.load(req.media)["display_name"]
        except ValidationError as e:
            raise falcon.HTTPBadRequest(str(e))

        try:
            user = User.get(req.context.user_id)
            user.display_name = display_name
            user.last_login = datetime.utcnow()
        except User.DoesNotExist:
            user = User(uuid=req.context.user_id, display_name=display_name)
        user.save()


class UserByUuidResource(object):

    schema = UserSchema()

    def on_get(self, _req: Request, resp: Response, uuid: str):
        try:
            user = User.get(uuid)
            resp.media = self.schema.dump(user)
        except User.DoesNotExist:
            raise falcon.HTTPNotFound()


def create():
    class CORSComponent(object):
        def process_response(self, req, resp, resource, req_succeeded):
            resp.set_header('Access-Control-Allow-Origin', '*')

    class AuthComponent(object):
        def process_request(self, request: Request, response: Response):
            try:
                user_id = request.env["awsgi.event"]["requestContext"]["authorizer"]["principalId"]
                request.context.user_id = user_id
            except KeyError:
                raise falcon.HTTPUnauthorized()

    app = API(middleware=[CORSComponent(), AuthComponent()])
    app.add_route("/users/v1/account", MyAccountResource())
    app.add_route("/users/v1/u/{uuid}", UserByUuidResource())
    return app
