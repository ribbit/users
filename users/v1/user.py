from os import environ
from datetime import datetime

from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute, UTCDateTimeAttribute


class User(Model):
    class Meta:
        read_capacity_units = 1
        write_capacity_units = 1
        table_name = environ["USERS_TABLE_NAME"]

    uuid = UnicodeAttribute(hash_key=True)
    display_name = UnicodeAttribute(null=False)
    created = UTCDateTimeAttribute(null=False, default=lambda: datetime.utcnow())
    last_login = UTCDateTimeAttribute(null=False, default=lambda: datetime.utcnow())
