test:
	python -m pytest --cov=users --cov-report term-missing --cov-fail-under 100 --flake8 tests

package:
	sam build
	sam package --s3-bucket io.andrewohara.pipeline --output-template-file .aws-sam/out.yml

deploy: package
	aws cloudformation deploy --template-file .aws-sam/out.yml --stack-name ribbit-users-dev --capabilities CAPABILITY_IAM