from datetime import datetime, timezone

import pytest
from moto import mock_dynamodb2
from falcon.testing import TestClient as FalconClient
from freezegun import freeze_time

from users import APP
from users.v1.user import User


@pytest.fixture(name="client")
def client_fixture() -> FalconClient:
    with mock_dynamodb2():
        User.create_table(read_capacity_units=1, write_capacity_units=1)
        yield FalconClient(APP)


@pytest.fixture(name="user1")
def user1_fixture() -> User:
    user = User(
        uuid="123",
        display_name="User 1",
        created=datetime(2018, 6, 1),
        last_login=datetime(2018, 6, 1)
    )
    user.save()
    return user


@pytest.fixture(name="user2")
def user2_fixture() -> User:
    user = User(
        uuid="456",
        display_name="User 2",
        created=datetime(2018, 6, 2),
        last_login=datetime(2018, 6, 2)
    )
    user.save()
    return user


def user_env(user_id: str) -> dict:
    return {"awsgi.event": {"requestContext": {"authorizer": {"principalId": user_id}}}}


@freeze_time("2019-09-09")
def test_get_missing(client: FalconClient):
    r = client.simulate_get(
        "/users/v1/u/user1",
        extras=user_env("user1")
    )
    assert 404 == r.status_code


@freeze_time("2019-09-09")
def test_get_unauthorized(client: FalconClient, user1: User):
    r = client.simulate_get(f"/users/v1/u/{user1.uuid}",)
    assert r.status_code == 401


@freeze_time("2019-09-09")
def test_get_different_user(client: FalconClient, user1: User, user2: User):
    response = client.simulate_get(
        f"/users/v1/u/{user2.uuid}",
        extras=user_env(user1.uuid)
    )
    assert response.status_code == 200

    assert response.json == dict(
        id="456",
        display_name="User 2",
        created="2018-06-02T00:00:00+00:00",
        last_login="2018-06-02T00:00:00+00:00"
    )


@freeze_time("2019-09-09")
def test_get_same_user(client: FalconClient, user1: User):
    response = client.simulate_get(
        f"/users/v1/u/{user1.uuid}",
        extras=user_env(user1.uuid)
    )
    assert response.status_code == 200

    assert response.json == dict(
        id="123",
        display_name="User 1",
        created="2018-06-01T00:00:00+00:00",
        last_login="2018-06-01T00:00:00+00:00"
    )


@freeze_time("2019-09-09")
def test_login_first_login(client: FalconClient):
    response = client.simulate_post(
        "/users/v1/account",
        json=dict(display_name="User 3"),
        extras=user_env("789")
    )
    assert response.status_code == 200

    user = User.get("789")
    assert user.uuid == "789"
    assert user.display_name == "User 3"
    assert user.created == datetime(2019, 9, 9).replace(tzinfo=timezone.utc)
    assert user.last_login == datetime(2019, 9, 9).replace(tzinfo=timezone.utc)


@freeze_time("2019-09-09")
def test_login(client: FalconClient, user1: User):
    response = client.simulate_post(
        "/users/v1/account",
        json=dict(display_name=user1.display_name),
        extras=user_env(user1.uuid)
    )
    assert response.status_code == 200

    user = User.get(user1.uuid)
    assert user.uuid == user1.uuid
    assert user.display_name == user1.display_name
    assert user.created == datetime(2018, 6, 1).replace(tzinfo=timezone.utc)
    assert user.last_login == datetime(2019, 9, 9).replace(tzinfo=timezone.utc)


@freeze_time("2019-09-09")
def test_login_change_display_name(client: FalconClient, user1: User):
    response = client.simulate_post(
        "/users/v1/account",
        json=dict(display_name="New Name"),
        extras=user_env(user1.uuid)
    )
    assert response.status_code == 200

    user = User.get(user1.uuid)
    assert user.uuid == user1.uuid
    assert user.display_name == "New Name"
    assert user.created == datetime(2018, 6, 1).replace(tzinfo=timezone.utc)
    assert user.last_login == datetime(2019, 9, 9).replace(tzinfo=timezone.utc)


@freeze_time("2019-09-09")
def test_login_missing_display_name(client: FalconClient, user1: User):
    response = client.simulate_post(
        "/users/v1/account",
        extras=user_env(user1.uuid),
        json=dict()
    )
    assert response.status_code == 400

    user = User.get(user1.uuid)
    assert user.last_login == datetime(2018, 6, 1).replace(tzinfo=timezone.utc)


@freeze_time("2019-09-09")
def test_login_unauthorized(client: FalconClient):
    response = client.simulate_post(
        "/users/v1/account",
        json=dict(display_name="foo")
    )
    assert response.status_code == 401
